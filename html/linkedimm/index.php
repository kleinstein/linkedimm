<!DOCTYPE html>
<html>
<head>
    <title>LinkedImm A Linked Data Approach for Systems Vaccinology.</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	 <link rel="stylesheet" href="style.css">
	  
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">
<script src="script.js"></script>


  </head>

  <body>
  
  
<div class="container">
<p>
<p>
<p>
<p>
<p>
<p>
<p><img src="images/logo.png" width="350" height="98"  alt=""/><strong><em>DASHBOARD</em></strong>
<p><strong>A Linked Data Approach for Systems Vaccinology.</strong></p>
<div align="left">
<br>
 <div class="alert alert-success">
  <strong>What is LinkedImm?</strong> Systems vaccinology studies combine high-throughput experimental  profiling techniques to provide an integrated, dynamic view of  vaccine-driven immune responses. We introduce  LinkedImm, which integrates heterogeneous data generated from  profiling the human response to influenza vaccination data along with  other public biological datasets. Integration of heterogeneous data is  challenging because of their diverse data formats. To facilitate data  integration, we designed a data model which logically arranges the key  data points such as genes, pathways, transcriptional profiling data  available at various public repositories (e.g., Immport, ImmuneSpace,  Reactome and Gene Ontology). These heterogeneous data are then  made available in a unified format through a knowledge graph. Our ongoing work on LinkedImm dashboard will enable the scientific  community to easily explore theses integrated data.</cite></p> 
  <strong><A href="LinkedImm_final.pdf">View LinkedImm Poster</a> </strong></div>
  


<div class="alert alert-info">
    <strong>LinkedImm Demo</strong> 

  <div align="center"> <a href="http://128.36.40.192/linkedimmdashboard/" class="btn btn-info" role="button">Demographics</a>
<a href="ontologydemo.php" class="btn btn-primary" role="button">HAI Analysis</a>
<a href="nli/index.php" class="btn btn-warning" role="button">Natural Language Interface Demo</a>
</div>
  </div>

  </body>
</html>